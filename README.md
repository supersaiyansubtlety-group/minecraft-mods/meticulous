<!--required for Modrinth centering -->
<center>
<div style="text-align:center;line-height:250%">

## Meticulous

[![Minecraft versions](https://cf.way2muchnoise.eu/versions/Minecraft_meticulous_all.svg)](https://modrinth.com/mod/meticulous/versions#all-versions)
![environment: client](https://img.shields.io/badge/environment-client-1976d2)  
[![loader: Fabric](https://img.shields.io/badge/loader-Fabric-cdc4ae?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjYgMjgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48cGF0aCBkPSJtMTAgMjQtOC04TDE0IDRWMmgybDYgNnY0TDEwIDI0eiIgc3R5bGU9ImZpbGw6I2RiZDBiNCIvPjxwYXRoIGQ9Ik0xMiA2djJoLTJ2Mkg4djJINnYySDR2MkgydjRoMnYyaDJ2MmgydjJoMnYtMmgydi0yaDJ2LTJoMnYtMmgydi0yaDJ2LTRoLTJ2LTJoLTJWOGgtMlY2aDJ2MmgydjJoMnYyaDJ2Mmgydi0yaC0yVjhoLTJWNmgtMlY0aC0yVjJoMnYyaDJ2MmgydjJoMnYyaDJ2NGgtMnYyaC00djJoLTJ2MmgtMnYyaC0ydjRoLTJ2Mkg4di0ySDZ2LTJINHYtMkgydi0ySDB2LTRoMnYtMmgydi0yaDJ2LTJoMlY4aDJWNmgyVjJoMlYwaDJ2MmgtMnY0aC0yIiBzdHlsZT0iZmlsbDojMzgzNDJhIi8+PHBhdGggc3R5bGU9ImZpbGw6IzgwN2E2ZCIgZD0iTTIyIDEyaDJ2MmgtMnoiLz48cGF0aCBkPSJNMiAxOGgydjJoMnYyaDJ2MmgydjJIOHYtMkg2di0ySDR2LTJIMnYtMiIgc3R5bGU9ImZpbGw6IzlhOTI3ZSIvPjxwYXRoIGQ9Ik0yIDE2aDJ2MmgydjJoMnYyaDJ2Mkg4di0ySDZ2LTJINHYtMkgydi0yeiIgc3R5bGU9ImZpbGw6I2FlYTY5NCIvPjxwYXRoIGQ9Ik0yMiAxMnYtMmgtMlY4aC0yVjZsLTIuMDIzLjAyM0wxNiA4aDJ2MmgydjJoMnpNMTAgMjR2LTJoMnYtNGg0di00aDJ2LTJoMnY0aC0ydjJoLTJ2MmgtMnYyaC0ydjJoLTIiIHN0eWxlPSJmaWxsOiNiY2IyOWMiLz48cGF0aCBkPSJNMTQgMThoLTR2LTJIOHYtMmgydjJoNHYyem00LTRoLTR2LTJoLTJ2LTJoLTJWOGgydjJoMnYyaDR2MnpNMTQgNGgydjJoLTJWNHoiIHN0eWxlPSJmaWxsOiNjNmJjYTUiLz48L3N2Zz4=)](https://fabricmc.net/)
<a href="https://quiltmc.org/"><img alt="available for: Quilt Loader" width=73 src="https://raw.githubusercontent.com/QuiltMC/art/master/brand/1024png/quilt_available_dark.png"></a>  
<a href="https://modrinth.com/mod/fabric-api/versions"><img alt="Requires: Fabric API" width="60" src="https://i.imgur.com/Ol1Tcf8.png"></a>
[![supports: Cloth Config](https://img.shields.io/badge/supports-Cloth_Config-89dd43?logo=data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj48c3ZnIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHZpZXdCb3g9IjAgMCAyNjQgMjY0IiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zOnNlcmlmPSJodHRwOi8vd3d3LnNlcmlmLmNvbS8iIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MjsiPjxwYXRoIGlkPSJiYWNrZ3JvdW5kIiBkPSJNODQsMjA3bC0zNiwtMzZsMCwtMzlsMTgsLTE4bDE4LC0yMWwyMSwtMThsMzYsLTM2bDAsLTIxbDIxLDBsMTgsMjFsMzYsMzZsMCwxOGwtNTQsNzhsLTM5LDM2bC0zOSwwWiIgc3R5bGU9ImZpbGw6IzljZmY1NTsiLz48cGF0aCBpZD0ib3V0bGluZSIgZD0iTTE2MiwxOGwwLC0xOGwtMjEsMGwwLDE4bC0xOCwwbDAsMzlsLTE4LDBsMCwxOGwtMjEsMGwwLDE4bC0xOCwwbDAsMjFsLTE4LDBsMCwxOGwtMTgsMGwwLDE4bC0yMSwwbDAsMzlsMjEsMGwwLDE4bDE4LDBsMCwxOGwxOCwwbDAsMjFsMTgsMGwwLDE4bDIxLDBsMCwtMThsMTgsMGwwLC0yMWwxOCwwbDAsLTE4bDIxLDBsMCwtMThsMTgsMGwwLC0xOGwxOCwwbDAsLTIxbDM5LDBsMCwtMThsMTgsMGwwLC0zOWwtMTgsMGwwLC0xOGwtMjEsMGwwLC0xOGwtMTgsMGwwLC0xOGwtMTgsMGwwLC0yMWwtMTgsMGwwLDIxbDE4LDBsMCwxOGwxOCwwbDAsMThsMTgsMGwwLDM5bDIxLDBsMCwxOGwtMjEsMGwwLC0xOGwtMTgsMGwwLC0yMWwtMTgsMGwwLC0xOGwtMTgsMGwwLC0xOGwtMjEsMGwwLDE4bDIxLDBsMCwxOGwxOCwwbDAsMjFsMTgsMGwwLDM2bC0xOCwwbDAsMjFsLTE4LDBsMCwxOGwtMjEsMGwwLDE4bC0xOCwwbDAsMThsLTE4LDBsMCwyMWwtMjEsMGwwLC0yMWwtMTgsMGwwLC0xOGwtMTgsMGwwLC0xOGwtMTgsMGwwLC0zOWwxOCwwbDAsLTE4bDE4LDBsMCwtMThsMTgsMGwwLC0yMWwyMSwwbDAsLTE4bDE4LDBsMCwtMThsMTgsMGwwLC0zOWwyMSwwIiBzdHlsZT0iZmlsbDojMTE1MTEwOyIvPjxwYXRoIGlkPSJzaGFkZS0xIiBzZXJpZjppZD0ic2hhZGUgMSIgZD0iTTQ4LDE3MWwtMTgsMGwwLDE4bDE4LDBsMCwxOGwxOCwwbDAsMThsMTgsMGwwLDIxbDIxLDBsMCwtMjFsLTIxLDBsMCwtMThsLTE4LDBsMCwtMThsLTE4LDBsMCwtMThabTE2OCwtNTdsMjEsMGwwLDE4bC0yMSwwbDAsLTE4WiIgc3R5bGU9ImZpbGw6IzZlYWEzMzsiLz48cGF0aCBpZD0ic2hhZGUtMiIgc2VyaWY6aWQ9InNoYWRlIDIiIGQ9Ik00OCwxNTBsLTE4LDBsMCwyMWwxOCwwbDAsMThsMTgsMGwwLDE4bDE4LDBsMCwxOGwyMSwwbDAsLTE4bC0yMSwwbDAsLTE4bC0xOCwwbDAsLTE4bC0xOCwwbDAsLTIxWiIgc3R5bGU9ImZpbGw6IzgwY2MzZDsiLz48cGF0aCBpZD0ic2hhZGUtMyIgc2VyaWY6aWQ9InNoYWRlIDMiIGQ9Ik0xMDUsMjI1bDAsLTE4bDE4LDBsMCwtMzZsMzksMGwwLC0zOWwxOCwwbDAsLTE4bDE4LDBsMCwzNmwtMTgsMGwwLDIxbC0xOCwwbDAsMThsLTIxLDBsMCwxOGwtMTgsMGwwLDE4bC0xOCwwWm0xMTEsLTExMWwtMTgsMGwwLC0yMWwtMTgsMGwwLC0xOGwtMTgsMGwwLC0xOGwxOCwwbDAsMThsMTgsMGwwLDE4bDE4LDBsMCwyMVoiIHN0eWxlPSJmaWxsOiM4OWRjNDI7Ii8+PHBhdGggaWQ9InNoYWRlLTQiIHNlcmlmOmlkPSJzaGFkZSA0IiBkPSJNMTQxLDE3MWwtMzYsMGwwLC0yMWwtMjEsMGwwLC0xOGwyMSwwbDAsMThsMzYsMGwwLDIxWm0zOSwtMzlsLTM5LDBsMCwtMThsLTE4LDBsMCwtMjFsLTE4LDBsMCwtMThsMTgsMGwwLDE4bDE4LDBsMCwyMWwzOSwwbDAsMThabS0xOCwtNzVsLTIxLDBsMCwtMThsMjEsMGwwLDE4WiIgc3R5bGU9ImZpbGw6IzkwZWI0YjsiLz48L3N2Zz4=)](https://modrinth.com/mod/cloth-config/versions)
[![supports: Mod Menu](https://img.shields.io/badge/supports-Mod_Menu-134bfe?logo=data:image/webp+xml;base64,UklGRlIAAABXRUJQVlA4TEUAAAAv/8F/AA9wpv9T/M///McDFLeNpKT/pg8WDv6jiej/BAz7v+bbAKDn9D9l4Es/T/9TBr708/Q/ZeBLP0//c7Xiqp7z/QEA)](https://modrinth.com/mod/modmenu/versions)
[![supports: SSS Translate](https://img.shields.io/badge/supports-SSS_Translate-253137?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMzAwIDMwMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48dXNlIHhsaW5rOmhyZWY9IiNhIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIvPjxkZWZzPjxpbWFnZSBpZD0iYSIgd2lkdGg9IjMwMCIgaGVpZ2h0PSIzMDAiIHhsaW5rOmhyZWY9ImRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBU3dBQUFFc0NBWUFBQUI1Zlk1MUFBQUFDWEJJV1hNQUFBN0VBQUFPeEFHVkt3NGJBQUFGbUVsRVFWUjRuTzNkSWRLazFSbUc0ZmVrV3NWZ0NDSUdOU09pRUdoMC9wanNaRmhBWEZqQllLS3pnSWdZR0QwN2lLT1lpTUVnUUNGR0lVNEVMS0NwNnE2VHUrZTZGdkRXVTlWVjk4d3gvN2YyM251b2VMN1dlblBMZzN2dkZ6UHo4cFkzdVp0WGE2Mm4weU5PK3QzcEFRRFhFaXdnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJRU93Z0F6QkFqSXVkN3I3cjVsNWZhZmI3N01mVGcvNERUNC9QZUFCdlQwOTRMUjdCZXYxV3V2TE85MG13Ty9QUFhnU0FobUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRY2ErLzZmNkh2ZmV6TzkxK243MWRhLzE4ZXNRMS9QNTM4VzZ0OWYzcEVTZXR2ZmMrUFlLclBWOXJ2Ym5sd2IzM2k1bDVlY3ViM00ycnRkYlQ2UkVuZVJJQ0dZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VER1pXYitjbnJFQS9wMFp2NStlc1NWL2owek4vMndCVFB6eSsvLzZla1JqK2F5MXZyNjlJaEhVL29RMFZycnU1bjU3dlNPUi9QcjE0aTRNVTlDSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlMbnZ2cDlNakhsRG00d043NzQ5bjVrK25kenlnRDA4UGVFU1htZm5xOUFpTyt1dk12RHc5QXE3aFNRaGtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbHI3LzNzOUFpdTluYXQ5Zk10RCs2OVA1aVpqMjU1azd0NXQ5YjYvdlFJQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBRCtQNjI5OTR2VEk3amFQOWRhUDkzeTRONzdrNW41N0pZMzRWN1czbnVmSHNIVm5xKzEzdHp5NEsvL1lMMjg1VTI0Rjk4bEJESUVDOGdRTENCRHNJQU13UUl5QkF2SUVDd2dRN0NBRE1FQ01nUUx5QkFzSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQWpNdWQ3bjYrMXZyeVRyZUI5NVQvWVFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUU1iYWUrODczSDA5TS8rNXc5MzMzUmRyclI5dmVYRHYvY25NZkhiTG16RWZ6Y3pmVG8rNDBqY3o4NC9USTA2NlY3QzRqK2RyclRlblJ6eVN2ZmV6bWZuMjlJNHJ2VnByUFowZWNaSW5JWkFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdaZVorZS9wRVEvbzl6UHp4OU1qNE5GYzFsclBUbzk0Tkh2dnA1bjU2dlFPZURTZWhFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tISFplMzk5ZXNRRCt2RDBBSGhFbDVuNTgra1JBTmZ3SkFReUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJT015TTY5T2orQnE3MDRQZ0pNdWE2Mm4weU1BcnVGSkNHUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRURHL3dCN1lYaUZyYXU1NUFBQUFBQkpSVTVFcmtKZ2dnPT0iLz48L2RlZnM+PC9zdmc+)](https://modrinth.com/mod/sss-translate)  
[![license: MIT](https://img.shields.io/badge/license-MIT-white)](https://gitlab.com/supersaiyansubtlety-group/minecraft-mods/meticulous/-/blob/master/LICENSE)
[![source on: GitLab](https://img.shields.io/badge/source_on-GitLab-fc6e26?logo=gitlab)](https://gitlab.com/supersaiyansubtlety-group/minecraft-mods/meticulous)
[![issues: GitLab](https://img.shields.io/gitlab/issues/open-raw/supersaiyansubtlety-group/minecraft-mods/meticulous?label=issues&logo=gitlab)](https://gitlab.com/supersaiyansubtlety-group/minecraft-mods/meticulous/-/issues)
[![localized: Percentage](https://badges.crowdin.net/meticulous/localized.svg)](https://crwd.in/meticulous)  
[![Modrinth: Downloads](https://img.shields.io/modrinth/dt/meticulous?logo=modrinth&label=Modrinth&color=00ae5d)](https://modrinth.com/mod/meticulous/versions)
[![CurseForge: Downloads](https://img.shields.io/curseforge/dt/575504?logo=curseforge&label=CurseForge&color=f16437)](https://www.curseforge.com/minecraft/mc-mods/meticulous/files)  
[![chat: Discord](https://img.shields.io/discord/1006391289006280746?logo=discord&color=5964f3)](https://discord.gg/xABmPngXAH)
<a href="https://coindrop.to/supersaiyansubtlety"><img alt="coindrop.to me" width="82" style="border-radius:3px" src="https://coindrop.to/embed-button.png"></a>
<a href="https://ko-fi.com/supersaiyansubtlety"><img alt="Buy me a coffee" width="106" src="https://i.ibb.co/4gwRR8L/p.png"></a>

</div>
</center>

---

### Helps you avoid breaking blocks accidentally.

Works client-side and in single player.

Meticulous adds configurable minimum times to break your first and second blocks, along with a few other options.  
"First/second block" means the first/second block you break while you hold down left click continuously.

---

<details>

<summary>Configuration</summary>

Key binds can always be configured through Minecraft's controls settings.

Supports [Cloth Config](https://modrinth.com/mod/cloth-config/versions) and
[Mod Menu](https://modrinth.com/mod/modmenu/versions) for configuration, but neither is required.

Options will use their default values if [Cloth Config](https://modrinth.com/mod/cloth-config/versions) is absent.

If [Cloth Config](https://modrinth.com/mod/cloth-config/versions) is present, options can be configured either through
[Mod Menu](https://modrinth.com/mod/modmenu/versions) or by editing `config/meticulous.json` in your
instance folder (`.minecraft/` by default for the vanilla launcher).

#### Key binds
- Toggle; default: `m`
- Modifier; default: `<unbound>`
- Always slow modifier; default: `<unbound>`
- Always slow blocks in list toggle; default: `<unbound>`

#### Options
- Enable; default: `false`
  > Main mod toggle. This can be overriden with the modifier keys.

- Notify on toggle; default: `true`
  > Display a message above the hotbar when a key bind toggles the mod or the "Always slow blocks list".

- Minimum time to break first block; default: `10`
  > Minimum time (in game ticks) it takes to break your first block after you start holding down the attack key
  continuously (left click).

- Minimum time to break second block; default: `10`
  > Minimum time (in game ticks) it takes to break your second block after you start holding down the attack key
  continuously (left click).

- Exclude blocks with hardness 0; default: `false`
  > Exclude block that break instantly even with no tool, such as flowers.

- Enable for items:
  > Items that Meticulous should affect when you use them to mine.
    - Axes; default: `true`
    - Hoes; default: `true`
    - Pickaxes; default: `true`
    - Shears; default: `true`
    - Shovels; default: `true`
    - Swords; default: `false`
    - Additional items; default: `<none>`
      > Other items that Meticulous should affect that don't fit into the toggleable categories.
    - Excluded items; default: `<none>`
      > Items that Meticulous should not affect even if they fit into the toggleable categories.

- Always slow blocks list; default: `<none>`
  > Blocks that Meticulous should always affect while "Always slow blocks in list" is enabled.

- Always slow blocks in list; default: `true`
  > Toggles the "Always slow blocks list".

- Minimum time to break when "Always slow"; default: `20`
  > Minimum time (in game ticks) it takes to break blocks in the "Always slow blocks list" or while the
  "Always slow modifier" key is held.

</details>

---

<details>

<summary>Translations</summary>

[![localized: Percentage](https://badges.crowdin.net/meticulous/localized.svg)](https://crwd.in/meticulous)
[![supports: SSS Translate](https://img.shields.io/badge/supports-SSS_Translate-253137?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMzAwIDMwMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48dXNlIHhsaW5rOmhyZWY9IiNhIiB3aWR0aD0iMzAwIiBoZWlnaHQ9IjMwMCIvPjxkZWZzPjxpbWFnZSBpZD0iYSIgd2lkdGg9IjMwMCIgaGVpZ2h0PSIzMDAiIHhsaW5rOmhyZWY9ImRhdGE6aW1hZ2UvcG5nO2Jhc2U2NCxpVkJPUncwS0dnb0FBQUFOU1VoRVVnQUFBU3dBQUFFc0NBWUFBQUI1Zlk1MUFBQUFDWEJJV1hNQUFBN0VBQUFPeEFHVkt3NGJBQUFGbUVsRVFWUjRuTzNkSWRLazFSbUc0ZmVrV3NWZ0NDSUdOU09pRUdoMC9wanNaRmhBWEZqQllLS3pnSWdZR0QwN2lLT1lpTUVnUUNGR0lVNEVMS0NwNnE2VHUrZTZGdkRXVTlWVjk4d3gvN2YyM251b2VMN1dlblBMZzN2dkZ6UHo4cFkzdVp0WGE2Mm4weU5PK3QzcEFRRFhFaXdnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJRU93Z0F6QkFqSXVkN3I3cjVsNWZhZmI3N01mVGcvNERUNC9QZUFCdlQwOTRMUjdCZXYxV3V2TE85MG13Ty9QUFhnU0FobUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRY2ErLzZmNkh2ZmV6TzkxK243MWRhLzE4ZXNRMS9QNTM4VzZ0OWYzcEVTZXR2ZmMrUFlLclBWOXJ2Ym5sd2IzM2k1bDVlY3ViM00ycnRkYlQ2UkVuZVJJQ0dZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VER1pXYitjbnJFQS9wMFp2NStlc1NWL2owek4vMndCVFB6eSsvLzZla1JqK2F5MXZyNjlJaEhVL29RMFZycnU1bjU3dlNPUi9QcjE0aTRNVTlDSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlMbnZ2cDlNakhsRG00d043NzQ5bjVrK25kenlnRDA4UGVFU1htZm5xOUFpTyt1dk12RHc5QXE3aFNRaGtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbHI3LzNzOUFpdTluYXQ5Zk10RCs2OVA1aVpqMjU1azd0NXQ5YjYvdlFJQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBRCtQNjI5OTR2VEk3amFQOWRhUDkzeTRONzdrNW41N0pZMzRWN1czbnVmSHNIVm5xKzEzdHp5NEsvL1lMMjg1VTI0Rjk4bEJESUVDOGdRTENCRHNJQU13UUl5QkF2SUVDd2dRN0NBRE1FQ01nUUx5QkFzSUVPd2dBekJBaklFQzhnUUxDQkRzSUFNd1FJeUJBdklFQ3dnUTdDQWpNdWQ3bjYrMXZyeVRyZUI5NVQvWVFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUU1iYWUrODczSDA5TS8rNXc5MzMzUmRyclI5dmVYRHYvY25NZkhiTG16RWZ6Y3pmVG8rNDBqY3o4NC9USTA2NlY3QzRqK2RyclRlblJ6eVN2ZmV6bWZuMjlJNHJ2VnByUFowZWNaSW5JWkFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdaZVorZS9wRVEvbzl6UHp4OU1qNE5GYzFsclBUbzk0Tkh2dnA1bjU2dlFPZURTZWhFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tISFplMzk5ZXNRRCt2RDBBSGhFbDVuNTgra1JBTmZ3SkFReUJBdklFQ3dnUTdDQURNRUNNZ1FMeUJBc0lFT3dnQXpCQWpJRUM4Z1FMQ0JEc0lBTXdRSXlCQXZJRUN3Z1E3Q0FETUVDTWdRTHlCQXNJT015TTY5T2orQnE3MDRQZ0pNdWE2Mm4weU1BcnVGSkNHUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRUNHWUFFWmdnVmtDQmFRSVZoQWhtQUJHWUlGWkFnV2tDRllRSVpnQVJtQ0JXUUlGcEFoV0VDR1lBRVpnZ1ZrQ0JhUUlWaEFobUFCR1lJRlpBZ1drQ0ZZUUlaZ0FSbUNCV1FJRnBBaFdFQ0dZQUVaZ2dWa0NCYVFJVmhBaG1BQkdZSUZaQWdXa0NGWVFJWmdBUm1DQldRSUZwQWhXRURHL3dCN1lYaUZyYXU1NUFBQUFBQkpSVTVFcmtKZ2dnPT0iLz48L2RlZnM+PC9zdmc+)](https://modrinth.com/mod/sss-translate)  
Install [SSS Translate](https://modrinth.com/mod/sss-translate) to automatically download new translations.  
You can help translate Meticulous on [Crowdin](https://crwd.in/meticulous).

</details>

---

This mod is only for Fabric (works on Quilt, too!) and I won't be porting it to Forge. The license is MIT, however, so
anyone else is free to port it.

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
