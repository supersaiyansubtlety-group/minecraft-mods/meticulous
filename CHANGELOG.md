- 1.3.0 (10 Dec. 2024):
  - Updated for 1.21.2-1.21.4
  - Moderate internal changes
- 1.2.1 (5 Sep. 2024): Marked as compatible with 1.21.1
- 1.2.0 (13 Jul. 2024):
  - Updated for 1.21!
  - Replaced bundled [CrowdinTranslate](<https://github.com/gbl/CrowdinTranslate>) with optional
  [SSS Translate](<https://modrinth.com/mod/sss-translate>) dependency;
  install [SSS Translate](<https://modrinth.com/mod/sss-translate>) for automatic translation updates (client-side)
  - Improved config parsing
- 1.1.8 (24 May 2024): Updated for 1.20.5 and 1.20.6
- 1.1.7 (24 May 2024): Ported 1.1.6 fixes to MC 1.20.2-1.20.4
- 1.1.6 (24 May 2024):
  - Fixed a bug that could cause brief de-sync under very specific circumstance, usually involving falling blocks
(closes [#6](https://gitlab.com/supersaiyansubtlety-group/minecraft-mods/meticulous/-/issues/6))
  - Improved the way blocks are counted for the purposes of determining the first/second blocks broken
  - Moderate internal changes
- 1.1.5 (3 Feb. 2024):
  - Marked as compatible with 1.20.3 and 1.20.4
  - Fixed more "Mismatch in destroy block pos..." warnings (hopefully that's the last of them)
  - Changed default values of 'Enable' and 'Exclude hardness 0' to `false`
  - Minor internal changes
- 1.1.4 (20 Nov. 2023): Updated for 1.20.2
- 1.1.3 (22 Jun. 2023): Updated for 1.20 and 1.20.1!
- 1.1.2 (3 Apr. 2023): Updated for 1.19.4
- 1.1.1 (5 Aug. 2022): 
  - Marked as compatible with 1.19.2.
  - Increased maximum allowed minimum break time from 40 (2sec) to 100 (5sec).
- 1.1.0-b1 (3 Aug. 2022): Added "Always slow blocks list" and accompanying toggle key bind
- 1.0.5 (31 Jul. 2022): Fixed mixin apply error in production
- 1.0.4 (28 Jul. 2022): Updated for 1.19.1
- 1.0.3 (17 Jul. 2022): Fixed an issue where, after using the modifier key to slow block breaking, sometimes you couldn't break blocks normally until you used the modifier again.
- 1.0.2 (23 Jun. 2022): Updated for 1.19!
- 1.0.1 (23 Jun. 2022): Updated for 1.18.2!
- 1.0 (2 Feb. 2022): Initial release!
- 0.0.3 (1 Feb. 2022):
  
  - Added enabled config
  - Implemented config obeying
  - Implemented broken blocks counting
  - Implemented block breaking slowing

- 0.0.2 (30 Jan. 2022): Added configs and translations
- 0.0.1 (29 Jan. 2022):
  
  - Filled out README
  - Replaced all template placeholders with Meticulous-relevant equivalents