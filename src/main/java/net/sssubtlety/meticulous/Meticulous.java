package net.sssubtlety.meticulous;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.minecraft.text.Text;

public final class Meticulous {
	private Meticulous() { }

	public static final String NAMESPACE = "meticulous";

	public static final Logger LOGGER = LoggerFactory.getLogger(NAMESPACE);

    public static final Text NAME = Text.translatable("text." + NAMESPACE + ".name");
}
