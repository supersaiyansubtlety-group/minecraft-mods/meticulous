package net.sssubtlety.meticulous;

import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;
import net.fabricmc.loader.api.VersionParsingException;
import net.fabricmc.loader.api.metadata.version.VersionPredicate;
import net.minecraft.text.Text;

import java.util.Optional;

import static net.sssubtlety.meticulous.Meticulous.LOGGER;

public final class Util {
    private Util() { }

    public static Text replace(Text text, String regex, String replacement) {
        return Text.literal(text.getString().replaceAll(regex, replacement)).setStyle(text.getStyle());
    }

    public enum SlowBreaking {
        NONE,
        FIRST,
        SECOND,
        ALL
    }

    public static boolean isModLoaded(String id, String versionPredicate) {
        final Optional<ModContainer> optModContainer = FabricLoader.getInstance().getModContainer(id);
        if (optModContainer.isPresent()) {
            try {
                return VersionPredicate.parse(versionPredicate).test(optModContainer.get().getMetadata().getVersion());
            } catch (VersionParsingException e) {
                LOGGER.error("Error parsing version predicate", e);
            }
        }

        return false;
    }
}
