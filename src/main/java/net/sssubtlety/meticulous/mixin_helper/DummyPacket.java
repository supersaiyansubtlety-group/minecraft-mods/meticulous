package net.sssubtlety.meticulous.mixin_helper;

import net.minecraft.network.listener.PacketListener;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.PacketType;

public final class DummyPacket implements Packet<PacketListener> {
    public static final DummyPacket INSTANCE = new DummyPacket();

    private DummyPacket() { }

    @Override
    public PacketType<Packet<PacketListener>> getType() {
        return null;
    }

    @Override
    public void apply(PacketListener listener) { }
}
