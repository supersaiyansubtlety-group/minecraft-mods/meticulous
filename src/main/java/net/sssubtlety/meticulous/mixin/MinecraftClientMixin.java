package net.sssubtlety.meticulous.mixin;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.HitResult;

import net.sssubtlety.meticulous.config.ConfigManager;
import net.sssubtlety.meticulous.mixin_helper.ClientPlayerInteractionManagerMixinAccessor;

import org.jetbrains.annotations.Nullable;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(MinecraftClient.class)
abstract class MinecraftClientMixin {
    @Shadow
    @Nullable
    public ClientPlayerInteractionManager interactionManager;

    @Shadow
    @Nullable
    public HitResult crosshairTarget;

    @Shadow
    @Nullable
    public ClientWorld world;

    @Shadow
    @Nullable
    public ClientPlayerEntity player;

    @Inject(method = "handleBlockBreaking", at = @At("HEAD"))
    private void tryResetBrokenBlockCount(boolean completeBreak, CallbackInfo ci) {
        // noinspection DataFlowIssue; world is dereferenced elsewhere in the method, so it must not be null
        if (
            !completeBreak || !(
                ConfigManager.ALWAYS_SLOW_MODIFIER_KEY.isPressed() || (
                    this.crosshairTarget instanceof BlockHitResult blockHitResult &&
                    ConfigManager.blockIsAlwaysSlow(this.world.getBlockState(blockHitResult.getBlockPos()).getBlock())
                ) || (
                    ConfigManager.shouldSlowFirstBlocks(this.player)
                )
            )
        ) {
            // interactionManager is dereferenced elsewhere in the method, so it must not be null
            // noinspection DataFlowIssue
            ((ClientPlayerInteractionManagerMixinAccessor) this.interactionManager).meticulous$resetBrokenBlockCount();
        }
    }
}
