package net.sssubtlety.meticulous.mixin;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.injector.v2.WrapWithCondition;
import com.llamalad7.mixinextras.sugar.Share;
import com.llamalad7.mixinextras.sugar.ref.LocalBooleanRef;

import net.minecraft.block.BlockState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.c2s.play.PlayerActionC2SPacket;
import net.minecraft.network.packet.c2s.play.PlayerActionC2SPacket.Action;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

import net.sssubtlety.meticulous.config.ConfigManager;
import net.sssubtlety.meticulous.mixin_helper.ClientPlayerInteractionManagerMixinAccessor;
import net.sssubtlety.meticulous.mixin_helper.DummyPacket;
import net.sssubtlety.meticulous.Util.SlowBreaking;

import org.jetbrains.annotations.NotNull;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.Slice;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ClientPlayerInteractionManager.class)
abstract class ClientPlayerInteractionManagerMixin implements ClientPlayerInteractionManagerMixinAccessor {
    @Shadow
    @Final
    private MinecraftClient client;

    @Unique
    private SlowBreaking slowBreaking = SlowBreaking.NONE;

    @Unique
    private int brokenBlockCount;

    @Unique
    @NotNull
    BlockPos lastSentStartMiningPos = BlockPos.ORIGIN;

    @Unique
    private boolean instaMiningUpdateBlockBreakingProgress;

    @Override
    public void meticulous$resetBrokenBlockCount() {
        this.brokenBlockCount = 0;
    }

    @Inject(method = "breakBlock", at = @At("RETURN"))
    private void countBlockBroken(CallbackInfoReturnable<Boolean> cir) {
        if (cir.getReturnValue() && this.slowBreaking != SlowBreaking.NONE && this.slowBreaking != SlowBreaking.ALL) {
            this.brokenBlockCount++;
        }
    }

    // method is second (large) lambda passed to sendActionPacket in attackBlock
    // methods are for dev/prod differences
    @ModifyExpressionValue(
        method = { "m_ystmynus", "method_41930" },  require = 1, allow = 1,
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/block/BlockState;calcBlockBreakingDelta" +
                "(Lnet/minecraft/entity/player/PlayerEntity;Lnet/minecraft/world/BlockView;" +
                "Lnet/minecraft/util/math/BlockPos;)F"
        )
    )
    private float adjustDeltaAndUpdateState(
        float originalDelta, BlockState state, BlockPos pos, Direction direction, int i,
        @Share("preventInstaMine") LocalBooleanRef preventInstaMine,
        @Share("instaMiningAttackBlock") LocalBooleanRef instaMining
    ) {
        this.slowBreaking =
            ConfigManager.getSlowBreaking(this.brokenBlockCount, pos, this.client.player, this.client.world, state);

        final var adjustedDelta =
            ConfigManager.getAdjustedBlockBreakDelta(originalDelta, this.slowBreaking, state.getBlock());

        if (adjustedDelta.isPresent()) {
            preventInstaMine.set(originalDelta >= 1);
            instaMining.set(false);
            return adjustedDelta.get();
        } else {
            preventInstaMine.set(false);
            instaMining.set(originalDelta >= 1);
            return originalDelta;
        }
    }

    // method is second (large) lambda passed to sendActionPacket in attackBlock
    // methods are for dev/prod differences
    @Inject(method = { "m_ystmynus", "method_41930" }, require = 1, allow = 1, at = @At("TAIL"), cancellable = true)
    private void preventSlowedInstaMinePackets(
        BlockState state, BlockPos pos, Direction direction, int i,
        CallbackInfoReturnable<Packet<?>> cir,
        @Share("preventInstaMine") LocalBooleanRef preventInstaMine,
        @Share("instaMiningAttackBlock") LocalBooleanRef instaMining
    ) {
        // if state.isAir(), attackBlockAdjustDeltaAndUpdateState wasn't called
        if (state.isAir()) {
            this.slowBreaking = SlowBreaking.NONE;
        } else if (preventInstaMine.get()) {
            cir.setReturnValue(DummyPacket.INSTANCE);
        } else if (!instaMining.get()) {
            this.lastSentStartMiningPos = pos;
        }
    }

    @WrapWithCondition(
        method = "sendActionPacket", require = 1, allow = 1,
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/client/network/ClientPlayNetworkHandler;" +
                "send(Lnet/minecraft/network/packet/Packet;)V"
        )
    )
    private boolean preventSendingDummy(ClientPlayNetworkHandler instance, Packet<?> packet) {
        return packet != DummyPacket.INSTANCE;
    }

    @ModifyExpressionValue(
        method = "updateBlockBreakingProgress", require = 1, allow = 1,
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/block/BlockState;calcBlockBreakingDelta(" +
                "Lnet/minecraft/entity/player/PlayerEntity;" +
                "Lnet/minecraft/world/BlockView;" +
                "Lnet/minecraft/util/math/BlockPos;" +
                ")F"
        )
    )
    private float adjustDelta(float originalDelta, BlockPos pos, Direction direction) {
        // noinspection DataFlowIssue; this.client.world dereferenced before this method is called
        return ConfigManager.getAdjustedBlockBreakDelta(
            originalDelta, this.slowBreaking, this.client.world.getBlockState(pos).getBlock()
        ).orElse(originalDelta);
    }

    @ModifyExpressionValue(
        method = "updateBlockBreakingProgress",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/block/BlockState;calcBlockBreakingDelta" +
                "(Lnet/minecraft/entity/player/PlayerEntity;Lnet/minecraft/world/BlockView;" +
                "Lnet/minecraft/util/math/BlockPos;)F"
    ))
    private float captureDelta(float original) {
        this.instaMiningUpdateBlockBreakingProgress = original >= 1;

        return original;
    }

    // if pos doesn't equal lastSentStartMiningPos (because insta-mining was previously deferred), would cause de-sync:
    // send START_DESTROY_BLOCK instead of STOP_DESTROY_BLOCK to prevent de-sync
    @ModifyArg(
        // lambda passed to sendActionPacket in updateBlockBreakingProgress
        // methods are for dev/prod differences
        method = { "m_eibfvbml", "method_41932" }, require = 1,
        at = @At(
            value = "INVOKE", ordinal = 0,
            target = "Lnet/minecraft/network/packet/c2s/play/PlayerActionC2SPacket;<init>" +
                "(Lnet/minecraft/network/packet/c2s/play/PlayerActionC2SPacket$Action;" +
                "Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/util/math/Direction;I)V"
        ),
        slice = @Slice(
            from = @At(
                value = "FIELD",
                target = "Lnet/minecraft/network/packet/c2s/play/PlayerActionC2SPacket$Action;" +
                    "STOP_DESTROY_BLOCK:Lnet/minecraft/network/packet/c2s/play/PlayerActionC2SPacket$Action;"
            )
        )
    )
    private Action replaceStopWithStartIfPosMismatch(
        Action action, BlockPos pos, Direction direction, int sequence
    ) {
        if (!this.lastSentStartMiningPos.equals(pos)) {
            if (!this.instaMiningUpdateBlockBreakingProgress) {
                this.lastSentStartMiningPos = pos;
            }

            return Action.START_DESTROY_BLOCK;
        } else {
            return action;
        }
    }

    @WrapWithCondition(
        method = { "attackBlock", "cancelBlockBreaking" },
        require = 2, allow = 2,
        at = @At(
            value = "INVOKE", ordinal = 0,
            target = "Lnet/minecraft/client/network/ClientPlayNetworkHandler;" +
                "send(Lnet/minecraft/network/packet/Packet;)V"
        )
    )
    private boolean preventAbortIfPosMismatch(ClientPlayNetworkHandler instance, Packet<?> packet) {
        return this.lastSentStartMiningPos.equals(
            packet instanceof PlayerActionC2SPacket actionPacket ? actionPacket.getPos() : null
        );
    }
}
