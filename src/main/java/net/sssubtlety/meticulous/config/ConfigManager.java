package net.sssubtlety.meticulous.config;

import net.sssubtlety.meticulous.Util.SlowBreaking;
import net.sssubtlety.meticulous.config.Config.EnableForItems;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import it.unimi.dsi.fastutil.booleans.BooleanConsumer;
import org.jetbrains.annotations.Nullable;
import org.lwjgl.glfw.GLFW;

import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.fabricmc.fabric.api.tag.convention.v2.ConventionalItemTags;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.option.KeyBind;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.registry.tag.ItemTags;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;

import java.util.Collection;
import java.util.Optional;
import java.util.function.BooleanSupplier;
import java.util.function.Function;
import java.util.function.Predicate;

import static net.sssubtlety.meticulous.Meticulous.LOGGER;
import static net.sssubtlety.meticulous.Meticulous.NAME;
import static net.sssubtlety.meticulous.Meticulous.NAMESPACE;
import static net.sssubtlety.meticulous.Util.isModLoaded;

public final class ConfigManager {
    private ConfigManager() { }

    public static final int MIN_MIN_BREAK_TIME = 0;
    public static final int MAX_MIN_BREAK_TIME = 100;

    public static final String KEY_BIND_CATEGORY = "key.categories." + NAMESPACE;

    public static final KeyBind TOGGLE_KEY = registerKeyBind("toggle", GLFW.GLFW_KEY_M);
    public static final KeyBind MODIFIER_KEY = registerUnboundKeyBind("modifier");
    public static final KeyBind ALWAYS_SLOW_MODIFIER_KEY = registerUnboundKeyBind("always_slow_modifier");
    public static final KeyBind ALWAYS_SLOW_BLOCKS_TOGGLE_KEY = registerUnboundKeyBind("always_slow_blocks_toggle");

    private static final Config CONFIG;

    private static final Text ENABLED = Text.translatable("message." + NAMESPACE + ".enabled");
    private static final Text DISABLED = Text.translatable("message." + NAMESPACE +".disabled");
    private static final Text ALWAYS_SLOW_BLOCKS =
        Text.translatable("message." + NAMESPACE + ".always_slow_blocks");

    private static final ImmutableList<ToolPredicate> TOOL_PREDICATES = ImmutableList.of(
        new ToolPredicate(EnableForItems::axes, ItemTags.AXES),
        new ToolPredicate(EnableForItems::hoes, ItemTags.HOES),
        new ToolPredicate(EnableForItems::pickaxes, ItemTags.PICKAXES),
        new ToolPredicate(EnableForItems::shears, ConventionalItemTags.SHEAR_TOOLS),
        new ToolPredicate(EnableForItems::shovels, ItemTags.SHOVELS),
        new ToolPredicate(EnableForItems::swords, ItemTags.SWORDS)
    );

    private static boolean enable;

    private static float maxFirstBlockBreakDelta;
    private static float maxSecondBlockBreakDelta;
    private static float maxAlwaysSlowBlockBreakDelta;

    private static ImmutableSet<Item> additionalItems;
    private static ImmutableSet<Item> excludedItems;

    private static boolean alwaysSlowBlocksInList;
    private static ImmutableSet<Block> alwaysSlowBlocks;

    static {
        if (isModLoaded("cloth-config", ">=7.0.72")) {
            CONFIG = ClothConfig.register(ConfigManager::update);
        } else {
            CONFIG = DefaultConfig.INSTANCE;
        }

        update(CONFIG);

        ClientTickEvents.END_CLIENT_TICK.register(ConfigManager::onEndClientTick);
    }

    public static void init() { }

    private static ActionResult update(Config config) {
        enable = config.enable();

        final var validatedMaxFirstDelta = ValidatedMaxDelta.of(config, Config::minFirstBlockBreakTime);

        maxFirstBlockBreakDelta = validatedMaxFirstDelta.delta;

        final var validatedMaxSecondDelta = ValidatedMaxDelta.of(config,Config::minSecondBlockBreakTime);

        maxSecondBlockBreakDelta = validatedMaxSecondDelta.delta;

        additionalItems = createItemSet(config.enableForItems().additionalIdStrings());

        excludedItems = createItemSet(config.enableForItems().excludedIdStrings());

        alwaysSlowBlocksInList = config.alwaysSlowBlocksInList();

        alwaysSlowBlocks = createBlockSet(config.alwaysSlowBlockIdStrings());

        final var validatedMaxAlwaysSlowDelta = ValidatedMaxDelta.of(config, Config::minAlwaysSlowBlockBreakTime);

        maxAlwaysSlowBlockBreakDelta = validatedMaxAlwaysSlowDelta.delta;

        return validatedMaxFirstDelta.invalidConfig
            || validatedMaxSecondDelta.invalidConfig
            || validatedMaxAlwaysSlowDelta.invalidConfig ?
                ActionResult.FAIL : ActionResult.SUCCESS;
    }

    private static KeyBind registerUnboundKeyBind(String translationKeySuffix) {
        return registerKeyBind(translationKeySuffix, GLFW.GLFW_KEY_UNKNOWN);
    }

    private static KeyBind registerKeyBind(String translationKeySuffix, int defaultKeyCode) {
        return KeyBindingHelper.registerKeyBinding(new KeyBind(
            "key." + NAMESPACE + "." + translationKeySuffix,
            defaultKeyCode,
            KEY_BIND_CATEGORY
        ));
    }

    private static boolean isEnabled() {
        return enable;
    }

    public static boolean shouldNotifyOnToggle() {
        return CONFIG.notifyOnToggle();
    }

    public static float getMaxFirstBlockBreakDelta() {
        return maxFirstBlockBreakDelta;
    }

    public static float getMaxSecondBlockBreakDelta() {
        return maxSecondBlockBreakDelta;
    }

    public static float getMaxAlwaysSlowBlockBreakDelta() {
        return maxAlwaysSlowBlockBreakDelta;
    }

    public static Optional<Float> getAdjustedBlockBreakDelta(
        float originalDelta, SlowBreaking slowBreaking, Block block
    ) {
        return getMaxBlockBreakingDelta(slowBreaking, block)
            .filter(maxBlockBreakingDelta -> maxBlockBreakingDelta < originalDelta);
    }

    public static boolean blockIsAlwaysSlow(Block block) {
        return alwaysSlowBlocksInList && alwaysSlowBlocks.contains(block);
    }

    private static Optional<Float> getMaxBlockBreakingDelta(SlowBreaking slowBreaking, Block block) {
        if (blockIsAlwaysSlow(block)) {
            return Optional.of(maxAlwaysSlowBlockBreakDelta);
        } else {
            return Optional.ofNullable(switch (slowBreaking) {
                case FIRST -> maxFirstBlockBreakDelta;
                case SECOND -> maxSecondBlockBreakDelta;
                case ALL -> maxAlwaysSlowBlockBreakDelta;
                case NONE -> null;
            });
        }
    }

    public static boolean shouldExcludeHardness0() {
        return CONFIG.excludeHardness0();
    }

    private static boolean isAllowed(ItemStack stack) {
        final Item item = stack.getItem();
        if (excludedItems.contains(item)) {
            return false;
        }

        if (additionalItems.contains(item)) {
            return true;
        }

        return TOOL_PREDICATES.stream().noneMatch(predicate -> predicate.denies(stack, CONFIG.enableForItems()));
    }

    private static <T> ImmutableSet<T> createRegistryEntrySet(Collection<String> ids, Registry<T> registry) {
        return ids.stream()
            .flatMap(string -> {
                final Optional<Identifier> id = Optional.ofNullable(Identifier.tryParse(string));

                if (id.isEmpty()) {
                    LOGGER.error("Invalid identifier: {}", string);
                }

                return id.stream();
            })
            .flatMap(id -> {
                final Optional<T> entry = registry.getOrEmpty(id);

                if (entry.isEmpty()) {
                    LOGGER.error("No \"{}\" with id: {}", registry.getKey().getValue(), id);
                }

                return entry.stream();
            })
            .collect(ImmutableSet.toImmutableSet());
    }

    public static ImmutableSet<Item> createItemSet(Collection<String> ids) {
        return createRegistryEntrySet(ids, Registries.ITEM);
    }

    public static ImmutableSet<Block> createBlockSet(Collection<String> ids) {
        return createRegistryEntrySet(ids, Registries.BLOCK);
    }

    public static Function<Screen, ? extends Screen> getScreenFactory() {
        return CONFIG.screenFactory();
    }

    public static boolean shouldSlowFirstBlocks(ClientPlayerEntity player) {
        return (isEnabled() != MODIFIER_KEY.isPressed()) && isAllowed(player.getMainHandStack());
    }

    public static SlowBreaking getSlowBreaking(
        int brokenBlockCount, BlockPos pos, ClientPlayerEntity player, ClientWorld world, BlockState state
    ) {
        // method is not called if state.isAir(), don't check it here
        if (ALWAYS_SLOW_MODIFIER_KEY.isPressed()) {
            return getMaxAlwaysSlowBlockBreakDelta() > 0 && shouldAffectStateAt(state, pos, world) ?
                SlowBreaking.ALL : SlowBreaking.NONE;
        } else if (blockIsAlwaysSlow(state.getBlock())) {
            return getMaxAlwaysSlowBlockBreakDelta() > 0 ?
                SlowBreaking.ALL : SlowBreaking.NONE;
        } else {
            if (shouldSlowFirstBlocks(player)) {
                if (brokenBlockCount == 0) {
                    return getMaxFirstBlockBreakDelta() > 0 && shouldAffectStateAt(state, pos, world) ?
                        SlowBreaking.FIRST : SlowBreaking.NONE;
                } else if (brokenBlockCount == 1) {
                    return getMaxSecondBlockBreakDelta() > 0 && shouldAffectStateAt(state, pos, world) ?
                        SlowBreaking.SECOND : SlowBreaking.NONE;
                } else {
                    return SlowBreaking.NONE;
                }
            } else {
                return SlowBreaking.NONE;
            }
        }
    }

    private static boolean shouldAffectStateAt(BlockState state, BlockPos pos, ClientWorld world) {
        return !shouldExcludeHardness0() || state.getHardness(world, pos) > 0;
    }

    private static void onEndClientTick(MinecraftClient client) {
        if (client.world == null) {
            return;
        }

        final boolean dirtyEnable = tryToggle(
            TOGGLE_KEY, NAME,
            () -> enable = !enable, CONFIG::setEnable,
            client.player
        );

        final boolean dirtyListEnable = tryToggle(
            ALWAYS_SLOW_BLOCKS_TOGGLE_KEY, ALWAYS_SLOW_BLOCKS,
            () -> alwaysSlowBlocksInList = !alwaysSlowBlocksInList, CONFIG::setAlwaysSlowBlocksInList,
            client.player
        );

        if (dirtyEnable || dirtyListEnable) {
            CONFIG.save();
        }
    }

    /**
     * Checks if a {@link KeyBind#wasPressed} and updates this manager and {@link #CONFIG} if so.
     *
     * @param toggleKey the key that should be checked
     * @param messagePrefix the prefix to the message that should be sent to the passed
     *                      {@code player} in the case of a toggle
     * @param toggler a function that, in the case of a toggle, sets this manager's value and returns the result
     * @param setter a setter that, in the case of a toggle, saves the new value to {@link #CONFIG}
     * @param player a player who {@linkplain #shouldNotifyOnToggle() should be notified} in the case of a toggle
     *
     * @return {@code true} <i>iff</i> {@link #CONFIG} should {@link Config#save save}
     */
    private static boolean tryToggle(
        KeyBind toggleKey, Text messagePrefix,
        BooleanSupplier toggler, BooleanConsumer setter,
        @Nullable ClientPlayerEntity player
    ) {
        if (toggleKey.wasPressed()) {
            final boolean value = toggler.getAsBoolean();

            if (shouldNotifyOnToggle() && player != null) {
                player.sendMessage(messagePrefix.copy().append(": ").append(value ? ENABLED : DISABLED), true);
            }

            setter.accept(value);

            return true;
        }

        return false;
    }

    private record ValidatedMaxDelta(float delta, boolean invalidConfig) {
        public static ValidatedMaxDelta of(Config config, Function<Config, Integer> minBreakTimeGetter) {
            final int configuredMinBreakTime = minBreakTimeGetter.apply(config);

            final boolean validConfig = isValidMinBreakTime(configuredMinBreakTime);

            final int minBreakTime = validConfig ?
                configuredMinBreakTime : minBreakTimeGetter.apply(DefaultConfig.INSTANCE);

            return new ValidatedMaxDelta(ticksToDelta(minBreakTime), !validConfig);
        }

        private static float ticksToDelta(int ticks) {
            return ticks > 0 ? 1f/ticks : -1;
        }

        private static boolean isValidMinBreakTime(int breakTime) {
            return breakTime == MathHelper.clamp(breakTime, MIN_MIN_BREAK_TIME, MAX_MIN_BREAK_TIME);
        }
    }

    private record ToolPredicate(Predicate<EnableForItems> isToolEnabled, TagKey<Item> tag) {
        boolean denies(ItemStack stack, EnableForItems enableForItems) {
            return !this.isToolEnabled.test(enableForItems) && stack.isIn(this.tag);
        }
    }
}
