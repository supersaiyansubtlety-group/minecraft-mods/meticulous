package net.sssubtlety.meticulous.config;

import org.jetbrains.annotations.UnmodifiableView;

import net.minecraft.client.gui.screen.Screen;

import java.util.List;
import java.util.function.Function;

public interface Config {
    boolean enable();

    void setEnable(boolean enable);

    boolean notifyOnToggle();

    int minFirstBlockBreakTime();

    int minSecondBlockBreakTime();

    boolean excludeHardness0();

    EnableForItems enableForItems();

    boolean alwaysSlowBlocksInList();

    void setAlwaysSlowBlocksInList(boolean slow);

    @UnmodifiableView
    List<String> alwaysSlowBlockIdStrings();

    int minAlwaysSlowBlockBreakTime();

    Function<Screen, ? extends Screen> screenFactory();

    void save();

    interface EnableForItems {
        boolean axes();

        boolean hoes();

        boolean pickaxes();

        boolean shears();

        boolean shovels();

        boolean swords();

        @UnmodifiableView
        List<String> additionalIdStrings();

        @UnmodifiableView
        List<String> excludedIdStrings();
    }
}
