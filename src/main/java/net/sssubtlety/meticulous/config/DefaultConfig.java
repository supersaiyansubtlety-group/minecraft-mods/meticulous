package net.sssubtlety.meticulous.config;

import net.sssubtlety.meticulous.Meticulous;
import net.sssubtlety.meticulous.Util;

import com.google.common.collect.ImmutableList;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.Text;
import net.minecraft.util.Formatting;

import java.util.function.Function;

public final class DefaultConfig implements Config {
    static final DefaultConfig INSTANCE = new DefaultConfig();

    private static final ImmutableList<String> ALWAYS_SLOW_BLOCKS_ID_STRINGS = ImmutableList.of();

    private DefaultConfig() { }

    @Override
    public boolean enable() {
        return false;
    }

    @Override
    public void setEnable(boolean enable) { }

    @Override
    public boolean notifyOnToggle() {
        return true;
    }

    @Override
    public int minFirstBlockBreakTime() {
        return 10;
    }

    @Override
    public int minSecondBlockBreakTime() {
        return 10;
    }

    @Override
    public boolean excludeHardness0() {
        return true;
    }

    @Override
    public Config.EnableForItems enableForItems() {
        return EnableForItems.INSTANCE;
    }

    @Override
    public boolean alwaysSlowBlocksInList() {
        return true;
    }

    @Override
    public void setAlwaysSlowBlocksInList(boolean slow) { }

    @Override
    public ImmutableList<String> alwaysSlowBlockIdStrings() {
        return ALWAYS_SLOW_BLOCKS_ID_STRINGS;
    }

    @Override
    public int minAlwaysSlowBlockBreakTime() {
        return 20;
    }

    @Override
    public Function<Screen, ? extends Screen> screenFactory() {
        return NoConfigScreen::new;
    }

    @Override
    public void save() { }

    private static final class EnableForItems implements Config.EnableForItems {
        private static final EnableForItems INSTANCE = new EnableForItems();

        private static final ImmutableList<String> ADDITIONAL_ID_STRINGS = ImmutableList.of();
        private static final ImmutableList<String> EXCLUDED_ID_STRINGS = ImmutableList.of();

        private EnableForItems() { }

        @Override
        public boolean axes() {
            return true;
        }

        @Override
        public boolean hoes() {
            return true;
        }

        @Override
        public boolean pickaxes() {
            return true;
        }

        @Override
        public boolean shears() {
            return true;
        }

        @Override
        public boolean shovels() {
            return true;
        }

        @Override
        public boolean swords() {
            return false;
        }

        @Override
        public ImmutableList<String> additionalIdStrings() {
            return ADDITIONAL_ID_STRINGS;
        }

        @Override
        public ImmutableList<String> excludedIdStrings() {
            return EXCLUDED_ID_STRINGS;
        }
    }

    public static class NoConfigScreen extends Screen {
        // WHITE's color is not null
        @SuppressWarnings("DataFlowIssue")
        private static final int TITLE_COLOR = Formatting.WHITE.getColorValue();

        // RED's color is not null
        @SuppressWarnings("DataFlowIssue")
        private static final int MESSAGE_COLOR = Formatting.RED.getColorValue();

        private static final Text TITLE =
            Text.translatable("text." + Meticulous.NAMESPACE + ".no_config_screen.title");
        private static final Text MESSAGE =
            Text.translatable("text." + Meticulous.NAMESPACE + ".no_config_screen.message");

        private final Screen parent;

        public NoConfigScreen(Screen parent) {
            super(TITLE);

            this.parent = parent;
        }

        @Override
        public void render(GuiGraphics graphics, int mouseX, int mouseY, float delta) {
            super.render(graphics, mouseX, mouseY, delta);
            final int horizontalCenter = this.width / 2;

            graphics.drawCenteredShadowedText(
                MinecraftClient.getInstance().textRenderer,
                Util.replace(TITLE, "\\$\\{name\\}", Meticulous.NAME.getString()),
                horizontalCenter, this.height / 10,
                TITLE_COLOR
            );

            graphics.drawCenteredShadowedText(
                MinecraftClient.getInstance().textRenderer,
                MESSAGE,
                horizontalCenter, this.height / 2,
                MESSAGE_COLOR
            );
        }

        // the super method also dereferences this.client so it must not be null
        @SuppressWarnings("ConstantConditions")
        @Override
        public void closeScreen() {
            this.client.setScreen(this.parent);
        }
    }
}
