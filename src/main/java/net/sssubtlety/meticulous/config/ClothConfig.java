package net.sssubtlety.meticulous.config;

import net.sssubtlety.meticulous.Meticulous;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.ConfigHolder;
import me.shedaniel.autoconfig.annotation.ConfigEntry;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.UnmodifiableView;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.ActionResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

@me.shedaniel.autoconfig.annotation.Config(name = Meticulous.NAMESPACE)
public final class ClothConfig implements ConfigData, Config {
    private ClothConfig() { }

    @Nullable
    @ConfigEntry.Gui.Excluded
    private static ConfigHolder<ClothConfig> holder;

    @ConfigEntry.Gui.Tooltip()
    boolean enable = DefaultConfig.INSTANCE.enable();

    @ConfigEntry.Gui.Tooltip()
    boolean notify_on_toggle = DefaultConfig.INSTANCE.notifyOnToggle();

    @ConfigEntry.Gui.Tooltip()
    @ConfigEntry.BoundedDiscrete(min = ConfigManager.MIN_MIN_BREAK_TIME, max = ConfigManager.MAX_MIN_BREAK_TIME)
    int min_first_block_break_time = DefaultConfig.INSTANCE.minFirstBlockBreakTime();

    @ConfigEntry.Gui.Tooltip()
    @ConfigEntry.BoundedDiscrete(min = ConfigManager.MIN_MIN_BREAK_TIME, max = ConfigManager.MAX_MIN_BREAK_TIME)
    int min_second_block_break_time = DefaultConfig.INSTANCE.minSecondBlockBreakTime();

    @ConfigEntry.Gui.Tooltip()
    boolean exclude_hardness_0 = DefaultConfig.INSTANCE.excludeHardness0();

    @ConfigEntry.Gui.Tooltip()
    @ConfigEntry.Gui.CollapsibleObject
    EnableForItems enable_for_items = new EnableForItems();

    @ConfigEntry.Gui.Tooltip()
    boolean always_slow_blocks_in_list = DefaultConfig.INSTANCE.alwaysSlowBlocksInList();

    @ConfigEntry.Gui.Tooltip()
    List<String> always_slow_blocks = DefaultConfig.INSTANCE.alwaysSlowBlockIdStrings();

    @ConfigEntry.Gui.Tooltip()
    @ConfigEntry.BoundedDiscrete(min = ConfigManager.MIN_MIN_BREAK_TIME, max = ConfigManager.MAX_MIN_BREAK_TIME)
    int min_always_slow_block_break_time = DefaultConfig.INSTANCE.minAlwaysSlowBlockBreakTime();

    static Config register(Function<Config, ActionResult> mangerUpdater) {
        if (holder != null) {
            throw new IllegalStateException("Trying to re-register");
        }

        final ConfigHolder<ClothConfig> configHolder =
            AutoConfig.register(ClothConfig.class, GsonConfigSerializer::new);
        final ClothConfig config = configHolder.getConfig();

        configHolder.registerSaveListener((savedHolder, savedConfig) -> mangerUpdater.apply(savedConfig));
        configHolder.registerLoadListener((loadedHolder, loadedConfig) -> mangerUpdater.apply(loadedConfig));

        mangerUpdater.apply(config);

        holder = configHolder;

        return config;
    }

    @Override
    public boolean enable() {
        return this.enable;
    }

    @Override
    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    @Override
    public boolean notifyOnToggle() {
        return this.notify_on_toggle;
    }

    @Override
    public int minFirstBlockBreakTime() {
        return this.min_first_block_break_time;
    }

    @Override
    public int minSecondBlockBreakTime() {
        return this.min_second_block_break_time;
    }

    @Override
    public boolean excludeHardness0() {
        return this.exclude_hardness_0;
    }

    @Override
    public Config.EnableForItems enableForItems() {
        return this.enable_for_items;
    }

    @Override
    public boolean alwaysSlowBlocksInList() {
        return this.always_slow_blocks_in_list;
    }

    @Override
    public void setAlwaysSlowBlocksInList(boolean slow) {
        this.always_slow_blocks_in_list = slow;
    }

    @Override
    @UnmodifiableView
    public List<String> alwaysSlowBlockIdStrings() {
        return this.always_slow_blocks;
    }

    @Override
    public int minAlwaysSlowBlockBreakTime() {
        return this.min_always_slow_block_break_time;
    }

    @Override
    public Function<Screen, ? extends Screen> screenFactory() {
        return parent -> AutoConfig.getConfigScreen(ClothConfig.class, parent).get();
    }

    @Override
    public void save() {
        Objects
            .requireNonNull(holder, "Cannot save before registration")
            .save();
    }

    private static final class EnableForItems implements Config.EnableForItems {
        boolean axes = DefaultConfig.INSTANCE.enableForItems().axes();

        boolean hoes = DefaultConfig.INSTANCE.enableForItems().hoes();

        boolean pickaxes = DefaultConfig.INSTANCE.enableForItems().pickaxes();

        boolean shears = DefaultConfig.INSTANCE.enableForItems().shears();

        boolean shovels = DefaultConfig.INSTANCE.enableForItems().shovels();

        boolean swords = DefaultConfig.INSTANCE.enableForItems().swords();

        @ConfigEntry.Gui.Tooltip()
        List<String> additional = new ArrayList<>(DefaultConfig.INSTANCE.enableForItems().additionalIdStrings());

        @ConfigEntry.Gui.Tooltip()
        List<String> excluded = new ArrayList<>(DefaultConfig.INSTANCE.enableForItems().excludedIdStrings());

        @Override
        public boolean axes() {
            return this.axes;
        }

        @Override
        public boolean hoes() {
            return this.hoes;
        }

        @Override
        public boolean pickaxes() {
            return this.pickaxes;
        }

        @Override
        public boolean shears() {
            return this.shears;
        }

        @Override
        public boolean shovels() {
            return this.shovels;
        }

        @Override
        public boolean swords() {
            return this.swords;
        }

        @Override
        @UnmodifiableView
        public List<String> additionalIdStrings() {
            return this.additional;
        }

        @Override
        @UnmodifiableView
        public List<String> excludedIdStrings() {
            return this.excluded;
        }
    }
}
